package com.github.msemitkin;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import static com.github.msemitkin.DotFileUtils.writeDotFile;

public class Huffman {
    private final String orgStr;
    private String encodedStr;

    private final Map<Character, Integer> occurrenceCount;
    private final Map<Character, String> characterCode;
    private final Map<String, Character> codeCharacter;
    private final PriorityQueue<Node> pq;  // for MinHeap
    private int counter;  // Unique id assigned to each node
    private int treeSize;  // # of total nodes in the tree
    private Node root;

    public Map<Character, Integer> getOccurrenceCount() {
        return occurrenceCount;
    }

    public Map<Character, String> getCharacterCode() {
        return characterCode;
    }

    public Huffman(String orgStr, String dotFileName) {
        this.counter = 0;
        this.treeSize = 0;
        this.orgStr = orgStr;
        occurrenceCount = new HashMap<>();
        characterCode = new HashMap<>();
        codeCharacter = new HashMap<>();
        pq = new PriorityQueue<>(1, (n1, n2) -> {
            if (n1.weight() < n2.weight())
                return -1;
            else if (n1.weight() > n2.weight())
                return 1;
            return 0;
        });

        countWordFrequency();
        buildHuffmanTree();
        if (treeSize > 1) {
            writeDotFile(root, dotFileName);
        }
        buildHuffmanCodeTable();
    }

    private void buildHuffmanCodeTable() {
        buildCodeRecursion(root, "");  // Recursion
    }

    private void buildCodeRecursion(Node node, String code) {
        if (node != null) {
            if (isLeaf(node)) {
                characterCode.put(node.ch(), code);
                codeCharacter.put(code, node.ch());
            } else {
                buildCodeRecursion(node.left(), code + '0');
                buildCodeRecursion(node.right(), code + '1');
            }
        }
    }

    private void buildHuffmanTree() {
        buildMinHeap();  // Set all leaf nodes into MinHeap
        while (!pq.isEmpty()) {
            Node left = pq.poll();
            treeSize++;
            if (pq.peek() != null) {
                Node right = pq.poll();
                treeSize++;
                root = new Node('\0', left.weight() + right.weight(), left, right, ++counter);
            } else {  // only left child. right=null
                root = new Node('\0', left.weight(), left, null, ++counter);
            }

            if (pq.peek() != null) {
                pq.offer(root);
            } else {  // = Top root. Finished building the tree.
                treeSize++;
                break;
            }
        }
    }

    private void buildMinHeap() {
        for (Map.Entry<Character, Integer> entry : occurrenceCount.entrySet()) {
            Character ch = entry.getKey();
            Integer weight = entry.getValue();
            Node node = new Node(ch, weight, null, null, ++counter);
            pq.offer(node);
        }
    }

    private void countWordFrequency() {
        for (int i = 0; i < orgStr.length(); i++) {
            Character character = orgStr.charAt(i);
            int weight = occurrenceCount.containsKey(character) ? occurrenceCount.get(character) + 1 : 1;
            occurrenceCount.put(character, weight);
        }
    }

    private static boolean isLeaf(Node node) {
        return !node.hasLeftKid() && !node.hasRightKid();
    }

    public String encode() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < orgStr.length(); i++) {
            char ch = orgStr.charAt(i);
            sb.append(characterCode.get(ch));
        }
        encodedStr = sb.toString();
        return encodedStr;
    }

    public String decode() {
        StringBuilder sb = new StringBuilder();
        String t = "";

        for (int i = 0; i < encodedStr.length(); i++) {
            t += encodedStr.charAt(i);
            if (codeCharacter.containsKey(t)) {
                sb.append(codeCharacter.get(t));
                t = "";
            }
        }
        return sb.toString();
    }

}
