package com.github.msemitkin;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@SuppressWarnings("CallToPrintStackTrace")
class DotFileUtils {
    private DotFileUtils() {
    }

    static void writeDotFile(Node root, String fileName) {
        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
            printWriter.println("## Command to generate pdf:  dot -Tpdf test.dot -o test.pdf");
            printWriter.println("digraph g {");

            dotWriteRecursion(root, printWriter);

            printWriter.println("}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void dotWriteRecursion(Node node, PrintWriter printWriter) {
        if (isLeaf(node)) {
            return;
        }
        if (node.hasLeftKid()) {
            String t = "";
            char c = node.left().ch();
            if (c != '\0' && c != ' ' && c != '"' && c != '\n')  // regular characters
                t = "\\n " + c;
            else if (c == ' ')
                t = "\\n blank";
            else if (c == '"')  //escape "
                t = "\\n \\\"";
            else if (c == '\n')
                t = "\\n /n";
            printWriter.println(" \"" + node.uid() + "\\n" + node.weight() + "\" -> \"" + node.left().uid() + "\\n" + node.left().weight() + t + "\" [color=red, label=0]");
            dotWriteRecursion(node.left(), printWriter);
        }
        if (node.hasRightKid()) {
            String t = "";
            char c = node.right().ch();
            if (c != '\0' && c != ' ' && c != '"' && c != '\n') // regular characters
                t = "\\n " + c;
            else if (c == ' ')
                t = "\\n blank";
            else if (c == '"')  //escape
                t = "\\n \\\"";
            else if (c == '\n')
                t = "\\n /n";
            printWriter.println(" \"" + node.uid() + "\\" + "n" + node.weight() + "\" -> \"" + node.right().uid() + "\\n" + node.right().weight() + t + "\" [color=blue, label=1]");
            dotWriteRecursion(node.right(), printWriter);
        }
    }

    private static boolean isLeaf(Node node) {
        return !node.hasLeftKid() && !node.hasRightKid();
    }
}
