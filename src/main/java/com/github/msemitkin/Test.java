package com.github.msemitkin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {
    public static void testHuffman(String orgStr, boolean debug, String dotFileName) {
        System.out.print("* Building Huffman Tree and Code Tables...");
        Huffman huffman = new Huffman(orgStr, dotFileName);

        if (debug) {
            System.out.println("\n============= Word Frequency =============");
            for (Map.Entry<Character, Integer> entry : huffman.getOccurrenceCount().entrySet()) {
                String character = entry.getKey().toString();
                int frequency = entry.getValue();
                if (character.equals("\n"))
                    character = "\\n";
                System.out.println(character + " occurs " + frequency + " times");
            }

            System.out.println("\n========== Huffman Code for each character =============");
            for (Map.Entry<Character, String> entry : huffman.getCharacterCode().entrySet()) {
                String character = entry.getKey().toString();
                String code = entry.getValue();
                if (character.equals("\n"))
                    character = "\\n";
                System.out.println(character + ": " + code);
            }
            System.out.println();
        }

        System.out.print("* Encoding the text...");
        String encoded = huffman.encode();

        System.out.print("* Decoding the encoded text...");
        String decoded = huffman.decode();
        assertEquals(orgStr, decoded); // Check if original text and decoded text is exactly same

        double originalLength = orgStr.length() * 7.0;
        double encodedLength = encoded.length();
        System.out.println("\n========== RESULT ==========");
        System.out.println("Original string cost = " + (int) originalLength + " bits");
        System.out.println("Encoded  string cost = " + (int) encodedLength + " bits");
        double reduction = ((encodedLength - originalLength) / originalLength) * 100;
        System.out.println("% reduction = " + (-reduction));
    }

    public static void main(String[] args) throws IOException {
        boolean debug = true;
        String inputFile = args[0];
        System.out.println("Input file: " + inputFile);
        String outputDotFile = args[1];
        System.out.println("Output dot file: " + outputDotFile);

        System.out.print("* Loading the file...");

        String content;
        try (Stream<String> lines = Files.lines(Paths.get(inputFile))) {
            content = lines.collect(Collectors.joining("\n"));
        }

        testHuffman(content, debug, outputDotFile);

        System.out.println("\n----- Test DONE ----- ");
    }


    private static void assertEquals(String expected, String actual) {
        if (!expected.equals(actual)) {
            throw new IllegalArgumentException("Assert fail");
        }
    }
}
