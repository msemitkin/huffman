package com.github.msemitkin;

record Node(Character ch, int weight, Node left, Node right, int uid) {
    boolean hasLeftKid() {
        return left != null;
    }

    boolean hasRightKid() {
        return right != null;
    }
}
